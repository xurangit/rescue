// pages/map/index.js
var app = getApp();
Page({
  /**
   * 页面的初始数据
   */
  data: {
    longitude: '',
    latitude:'',
    longitude2:'',
    latitude2:'',
    phone:'4008120120',//紧急电话
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function () {
    var that = this;
  
    that.selfLocation();
      this.mapCtx = wx.createMapContext('map')
      this.mapCtx.moveToLocation();
  },
    onReady: function (e) {
        this.mapCtx = wx.createMapContext('map')
    },

  //获取当前的位置
  selfLocation:function(e){
    var that = this;
    //获取当前地址
    wx.getLocation({
      type: 'wgs84',
      success: function (res) {
      	var longitude2 = Math.floor(res.longitude)+'°'+Math.floor((res.longitude-Math.floor(res.longitude))*60)+"'"+Math.floor(((res.longitude-Math.floor(res.longitude))*60 - Math.floor((res.longitude-Math.floor(res.longitude))*60))*60)+"''";
      	var latitude2 = Math.floor(res.latitude)+'°'+Math.floor((res.latitude-Math.floor(res.latitude))*60)+"'"+Math.floor(((res.latitude-Math.floor(res.latitude))*60 - Math.floor((res.latitude-Math.floor(res.latitude))*60))*60)+"''";
        that.setData({
          longitude: res.longitude,
          latitude: res.latitude,
          longitude2: longitude2,
          latitude2: latitude2,
        });
      }
    });
  },
  //拨打紧急电话
  phone: function (e) {
    var that = this;
    wx.makePhoneCall({
      phoneNumber:that.data.phone,
    });
  },
  bindcontroltap: function (e) {
      this.movetoPosition();
  },

  goToShow:function() {
    wx.navigateTo({
      url: '../show/index'
    })
  },

    
    // 页面显示
    onShow: function(){
        // 1.创建地图上下文，移动当前位置到地图中心
        this.mapCtx = wx.createMapContext("map"); // 地图组件的id
        this.movetoPosition()
    },
// 定位函数，移动位置到地图中心
    movetoPosition: function(){
        this.mapCtx.moveToLocation();
    },
    getCenterLocation: function () {
        var that = this;
        this.mapCtx.getCenterLocation({
            success: function(res){
                that.setData({
                    centerLongitude: res.longitude,
                    centerLatitude: res.latitude
                });
            }
        })
    },
    moveToLocation: function () {
        this.mapCtx.moveToLocation({
            duration:1000
        })
    },
    
})